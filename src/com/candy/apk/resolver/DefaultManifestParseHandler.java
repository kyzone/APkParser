package com.candy.apk.resolver;

import com.candy.apk.domain.ApkInfo;
import com.candy.apk.domain.IApkInfo;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 * Created by yantingjun on 2015/3/22.
 */
public class DefaultManifestParseHandler extends ManifestParseHandler{
    private ApkInfo apkInfo = new ApkInfo();
    @Override
    public IApkInfo getApkInfo() {
        return apkInfo;
    }

    public void characters(char[] ch, int start, int length)
            throws SAXException
    {
        super.characters(ch, start, length);
    }

    public void endDocument()
            throws SAXException
    {
        super.endDocument();
    }

    public void endElement(String uri, String localName, String qName)
            throws SAXException
    {
        super.endElement(uri, localName, qName);
    }

    public void startDocument()
            throws SAXException
    {
        super.startDocument();
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException
    {
        if (qName.equals("manifest")) {
            for (int i = 0; i < attributes.getLength(); i++) {
                String strQname = attributes.getQName(i);
                if (strQname.equals("package")) {
                    this.apkInfo.setPackageName(attributes
                            .getValue(strQname));
                }else if (strQname.equals("android:versionName")) {
                    this.apkInfo.setVersionName(attributes
                            .getValue(strQname));
                }else if (strQname.equals("android:versionCode")) {
                    this.apkInfo.setVersionCode(attributes
                            .getValue(strQname));
                }
            }
        }else if(qName.equals("application")) {
            for (int i = 0; i < attributes.getLength(); i++) {
                String strQname = attributes.getQName(i);
                if (strQname.equals("android:icon")) {
                    this.apkInfo.setApplicationIcon(attributes
                            .getValue(strQname));
                }else if (strQname.equals("android:name")) {
                    this.apkInfo.setApplicationName(attributes
                            .getValue(strQname));
                }
            }
        }
        super.startElement(uri, localName, qName, attributes);
    }
}
