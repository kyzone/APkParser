package com.candy.apk.resolver;

import com.candy.apk.domain.IApkInfo;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Created by yantingjun on 2015/3/22.
 */
public abstract class ManifestParseHandler extends DefaultHandler{

    public abstract IApkInfo getApkInfo();
}
